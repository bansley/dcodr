from setuptools import setup

setup(name='Dcodr',
      version='1.1.21',
      description='''
        Application for tracking the surgical outcomes of procedures
      ''',
      author='Brett Ansley',
      author_email='brett.ansley@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',)
