from django.contrib.admin import ModelAdmin
from django.contrib.admin import StackedInline
from dcodr.admin import admin_site

from outcomes.models import OutcomeMeasure
from outcomes.models import OutcomeMeasureQuestion

from outcomes.models import OutcomeMeasureAnswer


class OutcomeMeasureQuestionInline(StackedInline):
    model = OutcomeMeasureQuestion
    extra = 0


class OutcomeMeasureAdmin(ModelAdmin):
    list_display = ('name',)
    inlines = [OutcomeMeasureQuestionInline]
    filter_horizontal = ['procedures']

admin_site.register(OutcomeMeasure, OutcomeMeasureAdmin)
admin_site.register(OutcomeMeasureAnswer)
