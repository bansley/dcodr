# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('outcomes', '0005_auto_20160112_1349'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='outcomemeasureanswer',
            options={'ordering': ['question__id']},
        ),
    ]
