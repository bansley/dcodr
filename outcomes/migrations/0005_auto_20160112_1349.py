# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('outcomes', '0004_auto_20160112_0147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='outcomemeasure',
            name='procedures',
            field=models.ManyToManyField(related_name='outcomemeasures', blank=True, to='core.Procedure'),
        ),
    ]
