# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('outcomes', '0002_outcomemeasureanswer'),
    ]

    operations = [
        migrations.CreateModel(
            name='OutcomeMeasureQuestion',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('question_text', models.CharField(max_length=40)),
                ('is_yes_no', models.BooleanField(verbose_name='is yes/no', help_text='If selected, minimum value and max value will be overwritten to 0 and 1')),
                ('min_value', models.PositiveIntegerField(verbose_name='minimum value', blank=True)),
                ('max_value', models.PositiveIntegerField(verbose_name='maximum value', blank=True)),
                ('outcome_measure', models.ForeignKey(to='outcomes.OutcomeMeasure')),
            ],
        ),
        migrations.RemoveField(
            model_name='outcomemesurequestion',
            name='outcome_measure',
        ),
        migrations.AlterField(
            model_name='outcomemeasureanswer',
            name='question',
            field=models.ForeignKey(to='outcomes.OutcomeMeasureQuestion'),
        ),
        migrations.DeleteModel(
            name='OutcomeMesureQuestion',
        ),
    ]
