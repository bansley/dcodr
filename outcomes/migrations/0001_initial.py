# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20160107_1949'),
    ]

    operations = [
        migrations.CreateModel(
            name='OutcomeMeasure',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=40)),
                ('procedures', models.ManyToManyField(to='core.Procedure')),
            ],
        ),
        migrations.CreateModel(
            name='OutcomeMesureQuestion',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('question_text', models.CharField(max_length=40)),
                ('is_yes_no', models.BooleanField()),
                ('min_value', models.PositiveIntegerField()),
                ('max_value', models.PositiveIntegerField()),
                ('outcome_measure', models.ForeignKey(to='outcomes.OutcomeMeasure')),
            ],
        ),
    ]
