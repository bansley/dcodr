# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('outcomes', '0003_auto_20160108_1728'),
    ]

    operations = [
        migrations.AlterField(
            model_name='outcomemeasure',
            name='procedures',
            field=models.ManyToManyField(related_name='outcomemeasures', to='core.Procedure'),
        ),
        migrations.AlterField(
            model_name='outcomemeasurequestion',
            name='max_value',
            field=models.PositiveIntegerField(default=5, blank=True, verbose_name='maximum value'),
        ),
        migrations.AlterField(
            model_name='outcomemeasurequestion',
            name='min_value',
            field=models.PositiveIntegerField(default=1, blank=True, verbose_name='minimum value'),
        ),
    ]
