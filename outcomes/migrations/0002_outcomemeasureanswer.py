# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20160107_1949'),
        ('outcomes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OutcomeMeasureAnswer',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('value', models.PositiveIntegerField()),
                ('question', models.ForeignKey(to='outcomes.OutcomeMesureQuestion')),
                ('survey', models.ForeignKey(to='core.Survey')),
            ],
        ),
    ]
