from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import ForeignKey
from django.db.models import ManyToManyField
from django.db.models import Model
from django.db.models import PositiveIntegerField
from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver

from core.models import Procedure
from core.models import Survey


class OutcomeMeasure(Model):
    name = CharField(max_length=40)
    procedures = ManyToManyField(
        Procedure, blank=True,
        related_name='outcomemeasures')


class OutcomeMeasureQuestion(Model):
    outcome_measure = ForeignKey(OutcomeMeasure)
    question_text = CharField(max_length=40)
    is_yes_no = BooleanField(
        verbose_name='is yes/no',
        help_text=('If selected, minimum value and max value '
                   'will be overwritten to 0 and 1'))
    min_value = PositiveIntegerField(blank=True,
                                     default=1, verbose_name='minimum value')
    max_value = PositiveIntegerField(blank=True,
                                     default=5, verbose_name='maximum value')

    def __str__(self):
        return self.question_text


class OutcomeMeasureAnswer(Model):
    survey = ForeignKey(Survey)
    question = ForeignKey(OutcomeMeasureQuestion)
    value = PositiveIntegerField()

    class Meta:
        ordering = ['question__id']

    def __str__(self):
        return '{} => {}: {}'.format(
            self.survey, self.question.question_text, self.value
        )


# ========= Signals ==========


@receiver(pre_save, sender=OutcomeMeasureQuestion)
def fix_max_min_values(sender, instance, *args, **kwargs):
    if instance.is_yes_no:
        instance.min_value = 0
        instance.max_value = 1
