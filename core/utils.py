from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string


def send_invitation_email(user):
    '''
    Sends invitation email to the user.

    Needs to have and email attribute in the user object.
    '''

    context = {'host': settings.HOST,
               'user': user}
    message = render_to_string('core/invitation_email.txt', context)
    html_message = render_to_string('core/invitation_email.html', context)
    send_mail(subject='Dcodr invitation',
              message=message,
              from_email='contact@dcodr.com',
              html_message=html_message,
              recipient_list=[user.email])
