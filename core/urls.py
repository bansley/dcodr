from django.conf.urls import url
from django.contrib.auth.views import login
from django.contrib.auth.views import logout
from password_validation.views import password_change
from django.contrib.auth.views import password_change_done
from django.contrib.auth.views import password_reset
from django.contrib.auth.views import password_reset_done
from django.contrib.auth.views import password_reset_complete

from password_validation.views import password_reset_confirm

from core.views import DataExportView
from core.views import HomeView
from core.views import InvitationAcceptView
from core.views import NewPatientView
from core.views import NewOperation
from core.views import NewSurveyResult
from core.views import PatientView
from core.views import PatientOperationsView
from core.views import SurveySelection
from core.views import SurveyEditView

from core.views import get_form_fields
from core.views import get_form_fields_and_data

urlpatterns = [
    url(r'^$', HomeView.as_view()),
    url(r'^patient/new/$', NewPatientView.as_view(), name='new_patient'),
    url(r'^patient/(?P<patient_id>\w{1,30})/add-operation/$',
        NewOperation.as_view(), name='new_operation'),
    url(r'^patient/(?P<patient_id>\w{1,30})/$',
        PatientOperationsView.as_view(), name='patient_operations'),
    url(r'^patient/(?P<patient_id>\w{1,30})/(?P<operation_id>\d+)/survey/$',
        NewSurveyResult.as_view(), name='new_survey'),
    url(r'^patient/(?P<patient_id>\w{1,30})/'
        r'(?P<operation_id>\d+)/survey/select/$',
        SurveySelection.as_view(), name='select_survey'),
    url(r'^patient/(?P<patient_id>\w{1,30})/'
        r'(?P<operation_id>\d+)/(?P<survey_id>\d+)/$',
        SurveyEditView.as_view(), name='select_survey'),
    url(r'^patient/$', PatientView.as_view(), name='patient'),
    url(r'^download/$',
        DataExportView.as_view(), name='data_export'),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$',
        logout,
        {'template_name': 'registration/logout.html'},
        name='logout'),
    url(r'^password_change/$',
        password_change,
        {'template_name': 'registration/password_change.html'},
        name='password_change'),
    url(r'^password_change_done/$',
        password_change_done,
        name='password_change_done'),
    url(r'^accept_invitation/(?P<user_token>\w+)/',
        InvitationAcceptView.as_view(),
        name='invitation_accept'),
    url(r'^password_reset/$',
        password_reset,
        {'template_name': 'registration/password_reset.html',
         'email_template_name': 'registration/email_password_reset.html',
         'subject_template_name': 'registration/subject_password_reset.txt'},
        name='password_reset'),
    url(r'^password_reset/done/$',
        password_reset_done,
        {'template_name': 'registration/password_reset_sent.html'},
        name='password_reset_done'),
    url(r'^password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/'
        r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        password_reset_confirm,
        {'template_name': 'registration/password_reset_confirmation.html'},
        name='password_reset_confirm'),
    url(r'^password_reset/complete/$',
        password_reset_complete,
        {'template_name': 'registration/password_reset_finished.html'},
        name='password_reset_complete'),
    url(r'^survey/(?P<outcome_measure_id>\d+)/$',
        get_form_fields, name='get_form_fields'),
    url(r'^survey/(?P<survey_id>\d+)/edit/$',
        get_form_fields_and_data, name='get_form_fields_and_data'),
]
