from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import CreateView
from django.views.generic import View

from core.forms import DataExport
from core.forms import OperationForm
from core.forms import PatientCreationForm
from core.forms import PatientSearchForm
from core.forms import SetPasswordForm

from core.forms import create_survey_form
from core.forms import create_survey_edit_form

from core.models import Operation
from core.models import Patient
from core.models import Procedure
from core.models import Survey
from core.models import Token

from outcomes.models import OutcomeMeasure
from outcomes.models import OutcomeMeasureAnswer


class DcodrMixin:
    #
    # This mixin is used to apply the decorators to
    # a class-based as_view() method.
    #
    @classmethod
    def as_view(cls, **initkwargs):
        view = super().as_view(**initkwargs)
        return login_required(view)


class HomeView(DcodrMixin, View):
    def __init__(self):
        self.template_name = 'core/index.html'
        self.context = {}

    def get(self, request, *args, **kwargs):
        self.context['form'] = DataExport()
        return render_to_response(
            self.template_name,
            self.context,
            RequestContext(request))


class NewPatientView(DcodrMixin, CreateView):
    template_name = 'core/patient_creation_form.html'
    form_class = PatientCreationForm
    success_url = '/patient/{patient_id}/add-operation/'


class NewOperation(DcodrMixin, View):
    def __init__(self):
        self.template_name = 'core/operation_creation_form.html'
        self.context = {}

    def get(self, request, patient_id):
        self.context['patient_id'] = patient_id
        self.context['form'] = OperationForm()
        return render_to_response(self.template_name,
                                  self.context,
                                  RequestContext(request))

    def post(self, request, patient_id):
        form = OperationForm(request.POST)
        if not form.is_valid():
            self.context['form'] = form
            self.context['patient_id'] = patient_id
            return render_to_response(self.template_name,
                                      self.context,
                                      RequestContext(request))
        operation = Operation()
        operation.patient = Patient.objects.get(patient_id=patient_id)
        operation.date = form.cleaned_data['date']
        operation.procedure_id = form.cleaned_data['procedure_id']
        operation.save()
        return redirect('/patient/{}/{}/survey/'.format(patient_id,
                                                        operation.id))


class NewSurveyResult(DcodrMixin, View):
    def __init__(self):
        self.template_name = 'core/survey_creation_form.html'
        self.context = {}

    def get(self, request, patient_id, operation_id):
        self.context['patient_id'] = patient_id
        self.context['operation_id'] = operation_id
        self.context['form_types'] = Operation.objects.get(
            pk=operation_id).procedure.outcomemeasures.all()
        return render_to_response(self.template_name,
                                  self.context,
                                  RequestContext(request))

    def post(self, request, patient_id, operation_id):
        data = request.POST.dict()
        data['operation'] = operation_id
        outcome = OutcomeMeasure.objects.get(pk=data.pop('survey'))
        form_class, fields = create_survey_form(outcome)
        form = form_class(data=data, fields=fields)
        if not form.is_valid():
            self.context['form'] = form
            return render_to_response(self.template_name,
                                      self.context,
                                      RequestContext(request))
        form.save()
        return redirect('/')


class PatientView(DcodrMixin, View):
    def __init__(self):
        self.template_name = 'core/patient.html'
        self.context = {}

    def get(self, request):
        self.context['form'] = PatientSearchForm()
        return render_to_response(self.template_name,
                                  self.context,
                                  RequestContext(request))

    def post(self, request):
        form = PatientSearchForm(request.POST)
        if not form.is_valid():
            self.context['form'] = form
            return render_to_response(self.template_name,
                                      self.context,
                                      RequestContext(request))
        patient_id = get_object_or_404(
            Patient,
            pk=form.cleaned_data['patient_id']).patient_id
        return redirect('/patient/{}/'.format(patient_id))


class PatientOperationsView(DcodrMixin, View):
    def __init__(self):
        self.template_name = 'core/operations.html'
        self.context = {}

    def get(self, request, patient_id):
        self.context['operations'] = Operation.objects.filter(
            patient__patient_id=patient_id)
        self.context['patient_id'] = patient_id
        return render_to_response(self.template_name,
                                  self.context,
                                  RequestContext(request))

    def post(self, request, patient_id):
        operation = get_object_or_404(
            Operation, pk=request.POST.get('operation'))
        return redirect('/patient/{}/{}/survey/'.format(
            patient_id, operation.id))


class SurveySelection(DcodrMixin, View):
    def __init__(self):
        self.template_name = 'core/select_survey.html'
        self.context = {}

    def get(self, request, patient_id, operation_id):
        self.context['surveys'] = Survey.objects.filter(
            operation__id=operation_id)
        self.context['patient_id'] = patient_id
        self.context['operation'] = Operation.objects.get(pk=operation_id)
        return render_to_response(self.template_name,
                                  self.context,
                                  RequestContext(request))


class SurveyEditView(DcodrMixin, View):
    def __init__(self):
        self.template_name = 'core/survey_edition_form.html'
        self.context = {}

    def get(self, request, patient_id, operation_id, survey_id):
        self.context['survey_id'] = survey_id
        outcome_measure = OutcomeMeasureAnswer.objects.filter(
            survey_id=survey_id).first().question.outcome_measure
        self.context['survey_date'] = Survey.objects.get(pk=survey_id).date
        self.context['form_type'] = outcome_measure.name
        self.context['form_type_id'] = outcome_measure.id
        return render_to_response(self.template_name,
                                  self.context,
                                  RequestContext(request))

    def post(self, request, patient_id, operation_id, survey_id):
        data = request.POST.dict()
        survey = Survey.objects.get(
            pk=survey_id)
        answers = survey.outcomemeasureanswer_set.order_by('question__id')
        form_class, fields = create_survey_edit_form(answers)
        form = form_class(data=data, fields=fields)
        if form.is_valid():
            form.save(survey)
            return redirect('/')
        self.context['form'] = form
        return render_to_response(self.template_name,
                                  self.context,
                                  RequestContext(request))


class DataExportView(DcodrMixin, View):
    def post(self, request):
        procedure = Procedure.objects.get(pk=request.POST.get('procedure'))
        surveys = Survey.objects.filter(operation__procedure=procedure)
        data = {}
        for survey in surveys:
            outcome = survey.outcomemeasureanswer_set.first()\
                .question.outcome_measure
            if outcome not in data:
                data[outcome] = []
            data[outcome].append(survey)
        data = render_to_string('core/data_export.csv', {'data': data})
        today = datetime.now().strftime('%Y%m%d')
        filename = procedure.code+today+'.csv'
        response = HttpResponse(data,  content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(
            filename)
        return response


class InvitationAcceptView(View):
    def __init__(self):
        self.template_name = 'core/invitation_accepted.html'
        self.context = {}

    def get(self, request, user_token):
        token = get_object_or_404(Token, value=user_token)
        self.context['user'] = token.user
        return render_to_response(
               self.template_name,
               self.context,
               RequestContext(request))

    def post(self, request, user_token):
        token = get_object_or_404(Token, value=user_token)
        form = SetPasswordForm(token.user, request.POST)
        if form.is_valid():
            form.save()
            token.user.is_active = True
            token.user.save()
            token.delete()
            return redirect('/login/')
        self.context['form'] = form
        return render_to_response(self.template_name,
                                  self.context,
                                  RequestContext(request))


# ========== AJAX ENDPOINTS ==========


def get_form_fields(request, outcome_measure_id):
    outcome = OutcomeMeasure.objects.get(pk=outcome_measure_id)
    data = []
    for question in outcome.outcomemeasurequestion_set.all():
        field = {
            'name': 'field{}'.format(question.id),
            'label': question.question_text,
            'yes_no': question.is_yes_no,
            'min_value': question.min_value,
            'max_value': question.max_value
        }
        data.append(field)
    return JsonResponse(data, safe=False)


def get_form_fields_and_data(request, survey_id):
    survey = Survey.objects.get(pk=survey_id)
    data = []
    for question in survey.outcomemeasureanswer_set.order_by('question__id'):
        field = {
            'name': 'field{}'.format(question.id),
            'label': question.question.question_text,
            'yes_no': question.question.is_yes_no,
            'min_value': question.question.min_value,
            'max_value': question.question.max_value,
            'value': question.value
        }
        data.append(field)
    return JsonResponse(data, safe=False)
