from django.forms import Form
from django.forms import CharField
from django.forms import DateField
from django.forms import IntegerField
from django.forms import ModelChoiceField
from django.forms import ModelForm
from django.forms import ValidationError
from django.utils.translation import ugettext_lazy as _

from password_validation import validation

from django_select2.forms import ModelSelect2Widget

from core.models import Operation
from core.models import Patient
from core.models import Procedure
from core.models import Survey

from outcomes.models import OutcomeMeasureAnswer


class PatientCreationForm(ModelForm):
    class Meta:
        model = Patient
        fields = ['patient_id']

    def clean_patient_id(self):
        if not self.cleaned_data['patient_id'].isalnum():
            raise ValidationError(
                'The patient ID should contain only letters and numbers.'
            )
        else:
            return self.cleaned_data['patient_id']


class OperationForm(ModelForm):
    procedure_id = CharField(widget=ModelSelect2Widget(
        model=Procedure,
        queryset=Procedure.objects.get_active(),
        search_fields=['code__icontains'],
        attrs={'class': 'form-control  col-sm-10', 'required': 'true'}
    ), required=True)

    class Meta:
        model = Operation
        fields = ['date', 'procedure_id']


class PatientSearchForm(Form):
    patient_id = CharField(widget=ModelSelect2Widget(
        model=Patient,
        search_fields=['patient_id__icontains'],
        attrs={'class': 'form-control  col-sm-10'}
    ))


class SetPasswordForm(Form):
    new_password1 = CharField(max_length=100)
    new_password2 = CharField(max_length=100)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data['new_password1']
        password2 = self.cleaned_data['new_password2']
        if password1 != password2:
            raise ValidationError(
                _("The two password fields didn't match."),
                code='password_mismatch',
            )
        validation.validate_password(password2, self.user)
        return password2

    def save(self):
        self.user.set_password(self.cleaned_data['new_password1'])


class DataExport(Form):
    procedure = CharField(widget=ModelSelect2Widget(
        model=Procedure,
        search_fields=['name'],
        attrs={
            'class': 'form-control',
            'required': True}
    ))


def create_survey_form(outcome_measure):
    def save(self):
        survey = Survey.objects.create(
            date=self.cleaned_data.pop('date'),
            operation=self.cleaned_data.pop('operation'))
        for field, data in self.cleaned_data.items():
            OutcomeMeasureAnswer.objects.create(
                question_id=field.split('field')[-1],
                survey=survey,
                value=data)

    class InternalForm(Form):
        def __init__(self, *args, **kwargs):
            fields = kwargs.pop('fields')
            super().__init__(*args, **kwargs)

            self.fields.update(fields)

    fields = {}
    fields['date'] = DateField()
    fields['operation'] = ModelChoiceField(Operation.objects.all())
    for question in outcome_measure.outcomemeasurequestion_set.all():
        fields['field{}'.format(question.id)] = IntegerField(
            min_value=question.min_value,
            max_value=question.max_value
        )
    InternalForm.save = save
    return InternalForm, fields


def create_survey_edit_form(outcome_measure_answers):
    def save(self, survey):
        survey.date = self.cleaned_data.pop('date')
        for field, data in self.cleaned_data.items():
            OutcomeMeasureAnswer.objects.filter(
                pk=field.split('field')[-1]).update(value=data)
        survey.save()

    class InternalForm(Form):
        def __init__(self, *args, **kwargs):
            fields = kwargs.pop('fields')
            super().__init__(*args, **kwargs)

            self.fields.update(fields)

    fields = {}
    fields['date'] = DateField()
    for question in outcome_measure_answers:
        fields['field{}'.format(question.id)] = IntegerField(
            min_value=question.question.min_value,
            max_value=question.question.max_value
        )
    InternalForm.save = save
    return InternalForm, fields
