(function(){
    if(!window.Global){
        window.Global = {};
    }
    if(!Global.angular_dependencies){
        Global.angular_dependencies = ['mod_service_survey'];
    }
    var survey_app = angular.module("survey-app", Global.angular_dependencies);
    survey_app.config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    });
    survey_app.controller('SurveyController',  ['$scope', 'SurveyModel', function($scope, SurveyModel){
        $scope.m = SurveyModel;
        var s_id = angular.element('#form_type_id').val();
        if(s_id){
            $scope.m.survey = s_id;
            $scope.m.load_form_and_data();
        }else{
            $scope.m.load_form();
        }
        $scope.m.loading = false;
    }]);
})();
