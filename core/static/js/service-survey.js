(function(){
    angular.module('mod_service_survey', ['ajax']);
    angular.module('mod_service_survey').factory('SurveyAPI', ['Ajax', function(Ajax){
        return {
            load_form: function(survey_id){
                return Ajax.get('/survey/'+survey_id+'/')
            },
            load_form_and_data: function(survey_id){
                return Ajax.get('/survey/'+survey_id+'/edit/')
            }
        }
    }]);
    angular.module('mod_service_survey').factory('SurveyModel', ['SurveyAPI', function(SurveyAPI){
        var m = {
            survey: "1"
        };
        m.load_form = function(){
            m.loading = true;
            SurveyAPI.load_form(m.survey).success(function(data){
                m.fields = data;
                m.loading = false;
            });
        };
        m.load_form_and_data = function(){
            m.loading = true;
            var survey_id = angular.element('#survey_id').val();
            SurveyAPI.load_form_and_data(survey_id).success(function(data){
                m.fields = data;
                m.loading = false;
            });
        };
        return m;
    }]);
})();
