angular.module('ajax', []);

angular.module('ajax').config(
    function ($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    }
);

angular.module('ajax').factory('Ajax', ['$http', function ($http) {
    return {
        get: function (url, params) {
            var key = localStorage.getItem("authorizarion_key");
            if(key){
                $http.defaults.headers.common.Authorization = 'Token token='+key;
            }
            if (!params) {
                params = {};
            }
            return $http({
                method: 'GET',
                url: url,
                params: params
            });
        },
        post: function (url, params) {
            if (!params) {
                params = {};
            }
            var data = {
                'method': 'POST',
                'url': url,
                'data': $.param(params)
            };
            return $http(data);
        },
        delete: function(url) {
            var data = {
                'method': 'DELETE',
                'url': url
            };
            return $http(data);
        },
        put: function(url, params) {
            if(!params){
                params = {};
            }
            var data = {
                'method': 'PUT',
                'url': url,
                'data': params
            };
            return $http(data);
        }
    };
}]);
