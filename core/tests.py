from django.test import TestCase
from django.test.utils import skipIf

from core.models import Token


class AddPatientTestCase(TestCase):
    fixtures = ['user.json']

    def setUp(self):
        self.client.login(username='admin', password='123')

    def test_access_page(self):
        response = self.client.get('/patient/new/')
        self.assertEqual(response.status_code, 200)

    def test_renders_right_template(self):
        response = self.client.get('/patient/new/')
        self.assertTemplateUsed(response, 'core/patient_creation_form.html')

    def test_creates_patient(self):
        patient = {'patient_id': '687f16sda16ad41fa6'}
        response = self.client.post('/patient/new/', data=patient)
        self.assertRedirects(response,
                             '/patient/687f16sda16ad41fa6/add-operation/')

    def test_create_empty_id(self):
        patient = {'patient_id': ''}
        response = self.client.post('/patient/new/', data=patient)
        self.assertContains(response, 'This field is required.')

    def test_sql_injection(self):
        patient = {'patient_id': '";select * from core_patients;'}
        response = self.client.post('/patient/new/', data=patient)
        self.assertContains(
            response,
            'The patient ID should contain only letters and numbers.'
        )

    def test_js_injection(self):
        patient = {'patient_id': 'alert("Hey!");'}
        response = self.client.post('/patient/new/', data=patient)
        self.assertContains(
            response,
            'The patient ID should contain only letters and numbers.'
        )

    def test_too_long_id(self):
        patient = {'patient_id': '0000000000000000000000000000000000000000000'}
        response = self.client.post('/patient/new/', data=patient)
        self.assertContains(
            response,
            'Ensure this value has at most 30 characters (it has 43).'
        )


class AddOperationTestCase(TestCase):
    fixtures = ['patient.json',
                'procedure.json',
                'user.json']

    def setUp(self):
        self.client.login(username='admin', password='123')

    def test_access_page(self):
        response = self.client.get('/patient/1231231231/add-operation/')
        self.assertEqual(response.status_code, 200)

    def test_page_content(self):
        response = self.client.get('/patient/1231231231/add-operation/')
        self.assertContains(response, 'Add operation to patient 1231231231')

    def test_create_operation(self):
        operation = {'date': '2015-02-28', 'procedure_id': '1'}
        response = self.client.post('/patient/1231231231/add-operation/',
                                    data=operation)
        self.assertRedirects(response, '/patient/1231231231/1/survey/')

    def test_empty_procedure_code(self):
        operation = {'date': '2015-02-28', 'procedure_id': ''}
        response = self.client.post('/patient/1231231231/add-operation/',
                                    data=operation)
        self.assertContains(response, 'This field is required.')

    def test_empty_date(self):
        operation = {'date': '', 'procedure_id': 'abc'}
        response = self.client.post('/patient/1231231231/add-operation/',
                                    data=operation)
        self.assertContains(response, 'This field is required.')

    def test_wrong_date(self):
        operation = {'date': '2000-02-31', 'procedure_code': 'abc'}
        response = self.client.post('/patient/1231231231/add-operation/',
                                    data=operation)
        self.assertContains(response, 'Enter a valid date.')


class AddSurveyTestCase(TestCase):
    fixtures = ['patient.json',
                'operation.json',
                'procedure.json',
                'user.json']

    def setUp(self):
        self.client.login(username='admin', password='123')

    @skipIf(True, 'Expecting story to be completed')
    def test_access_page(self):
        response = self.client.get('/patient/1231231231/1/survey/')
        self.assertEqual(response.status_code, 200)

    @skipIf(True, 'Expecting story to be completed')
    def test_create_survey(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 3,
            'self_care': 5,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertRedirects(response, '/')

    @skipIf(True, 'Expecting story to be completed')
    def test_create_empty_survey(self):
        survey = {
            'date': '',
            'mobility': '',
            'self_care': '',
            'usual_activities': '',
            'pain_discomfort': '',
            'anxiety_depression': '',
            'your_health': ''
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertRedirects(response, '/')

    @skipIf(True, 'Expecting story to be completed')
    def test_wrong_date(self):
        survey = {
            'date': '22/22/2015',
            'mobility': 3,
            'self_care': 5,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response, 'Enter a valid date.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_mobility(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 6,
            'self_care': 5,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_self_care(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 7,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_usual_activities(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 7,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_pain_discomfort(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 8,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_anxiety_depression(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 5,
            'anxiety_depression': 9,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_your_health(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 5,
            'anxiety_depression': 5,
            'your_health': 101
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 100.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_mobility(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 0,
            'self_care': 5,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_self_care(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 0,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_usual_activities(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 0,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_pain_discomfort(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 0,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_anxiety_depression(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 5,
            'anxiety_depression': 0,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_your_health(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 5,
            'anxiety_depression': 5,
            'your_health': -1
        }
        response = self.client.post('/patient/1231231231/1/survey/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 0.')


class PatientSelectionTestCase(TestCase):
    fixtures = ['patient.json',
                'user.json']

    def setUp(self):
        self.client.login(username='admin', password='123')

    def test_access_page(self):
        response = self.client.get('/patient/')
        self.assertEqual(response.status_code, 200)

    def test_select_patient(self):
        patient = {
            'patient_id': '2'
        }
        response = self.client.post('/patient/', data=patient)
        self.assertRedirects(response, '/patient/1231231231/')

    def test_unexistent_patient(self):
        patient = {
            'patient_id': '1'
        }
        response = self.client.post('/patient/', data=patient)
        self.assertEqual(response.status_code, 404)


class OperationSelectionTestCase(TestCase):
    fixtures = ['patient.json',
                'operation.json',
                'procedure.json',
                'user.json']

    def setUp(self):
        self.client.login(username='admin', password='123')

    def test_access_page(self):
        response = self.client.get('/patient/1231231231/')
        self.assertEqual(response.status_code, 200)

    def test_select_operation(self):
        operation = {
            'operation': '1'
        }
        response = self.client.post('/patient/1231231231/', data=operation)
        self.assertRedirects(response, '/patient/1231231231/1/survey/')

    def test_unexistent_operation(self):
        operation = {
            'operation': '2'
        }
        response = self.client.post('/patient/1231231231/', data=operation)
        self.assertEqual(response.status_code, 404)


class SurveySelectionTestCase(TestCase):
    fixtures = ['patient.json',
                'operation.json',
                'procedure.json',
                'user.json']

    def setUp(self):
        self.client.login(username='admin', password='123')

    @skipIf(True, 'Expecting story to be completed')
    def test_access_page(self):
        response = self.client.get('/patient/1231231231/1/survey/select/')
        self.assertEqual(response.status_code, 200)


class SurveyEditTestCase(TestCase):
    fixtures = ['patient.json',
                'operation.json',
                'procedure.json',
                'user.json']

    def setUp(self):
        self.client.login(username='admin', password='123')

    @skipIf(True, 'Expecting story to be completed')
    def test_access_page(self):
        response = self.client.get('/patient/1231231231/1/1/')
        self.assertEqual(response.status_code, 200)

    @skipIf(True, 'Expecting story to be completed')
    def test_edit_survey(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 3,
            'self_care': 5,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertRedirects(response, '/')

    @skipIf(True, 'Expecting story to be completed')
    def test_create_empty_survey(self):
        survey = {
            'date': '',
            'mobility': '',
            'self_care': '',
            'usual_activities': '',
            'pain_discomfort': '',
            'anxiety_depression': '',
            'your_health': ''
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertRedirects(response, '/')

    @skipIf(True, 'Expecting story to be completed')
    def test_wrong_date(self):
        survey = {
            'date': '22/22/2015',
            'mobility': 3,
            'self_care': 5,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response, 'Enter a valid date.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_mobility(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 6,
            'self_care': 5,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_self_care(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 7,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_usual_activities(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 7,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_pain_discomfort(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 8,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_anxiety_depression(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 5,
            'anxiety_depression': 9,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 5.')

    @skipIf(True, 'Expecting story to be completed')
    def test_higher_your_health(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 5,
            'anxiety_depression': 5,
            'your_health': 101
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is less than or equal to 100.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_mobility(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 0,
            'self_care': 5,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_self_care(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 0,
            'usual_activities': 3,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_usual_activities(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 0,
            'pain_discomfort': 4,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_pain_discomfort(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 0,
            'anxiety_depression': 5,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_anxiety_depression(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 5,
            'anxiety_depression': 0,
            'your_health': 68
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 1.')

    @skipIf(True, 'Expecting story to be completed')
    def test_lower_your_health(self):
        survey = {
            'date': '12/12/2015',
            'mobility': 5,
            'self_care': 5,
            'usual_activities': 5,
            'pain_discomfort': 5,
            'anxiety_depression': 5,
            'your_health': -1
        }
        response = self.client.post('/patient/1231231231/1/1/',
                                    data=survey)
        self.assertContains(response,
                            'Ensure this value is greater than or equal to 0.')


class DataExportTestCase(TestCase):
    fixtures = ['user.json']

    def setUp(self):
        self.client.login(username='admin', password='123')

    @skipIf(True, 'Expecting story to be completed')
    def test_access_page(self):
        response = self.client.get('/download/2001-01-01.csv')
        self.assertEqual(response.status_code, 200)

    @skipIf(True, 'Expecting story to be completed')
    def test_downloads_file(self):
        response = self.client.get('/download/2001-01-01.csv')
        self.assertIn(
            b'Content-Disposition: attachment; filename="2001-01-01.csv"',
            response.serialize_headers())


class PasswordChangeTestCase(TestCase):
    fixtures = ['user.json']

    def setUp(self):
        self.client.login(username='admin', password='123')

    def test_access_page(self):
        response = self.client.get('/password_change/')
        self.assertEqual(response.status_code, 200)

    def test_wrong_previous_password(self):
        password = {
            'old_password': '098',
            'new_password1': 'Pass123@',
            'new_password2': 'Pass123@'
        }
        response = self.client.post('/password_change/', data=password)
        self.assertContains(
            response,
            'Your old password was entered incorrectly. Please enter it again.'
        )

    def test_passwords_mismatch(self):
        password = {
            'old_password': '123',
            'new_password1': 'Pass123!',
            'new_password2': 'Pass123@'
        }
        response = self.client.post('/password_change/', data=password)
        self.assertContains(
            response,
            'The two password fields didn&#39;t match.'
        )

    def test_short_password(self):
        password = {
            'old_password': '123',
            'new_password1': 'Pass',
            'new_password2': 'Pass'
        }
        response = self.client.post('/password_change/', data=password)
        self.assertContains(
            response,
            'This password is too short. It must contain at least 8 characters'
        )

    def test_no_number(self):
        password = {
            'old_password': '123',
            'new_password1': 'Passabcd@',
            'new_password2': 'Passabcd@'
        }
        response = self.client.post('/password_change/', data=password)
        self.assertContains(
            response,
            'This password has no numbers.'
        )

    def test_no_symbol(self):
        password = {
            'old_password': '123',
            'new_password1': 'Passabcd12',
            'new_password2': 'Passabcd12'
        }
        response = self.client.post('/password_change/', data=password)
        self.assertContains(
            response,
            'This password has no symbols.'
        )

    def test_no_uppercase(self):
        password = {
            'old_password': '123',
            'new_password1': 'passabcd12@!',
            'new_password2': 'passabcd12@!'
        }
        response = self.client.post('/password_change/', data=password)
        self.assertContains(
            response,
            "This password has no uppercase letters."
        )

    def test_no_lowercase(self):
        password = {
            'old_password': '123',
            'new_password1': 'PASSABCD12@!',
            'new_password2': 'PASSABCD12@!'
        }
        response = self.client.post('/password_change/', data=password)
        self.assertContains(
            response,
            "This password has no lowercase letters."
        )

    def test_long_password(self):
        password = {
            'old_password': '123',
            'new_password1': 'Pass1234@!Pass1234@!Pass1234@!Pass1234@!'
                             'Pass1234@!Pass1234@!Pass1234@!Pass1234@!'
                             'Pass1234@!Pass1234@!Pass1234@!',
            'new_password2': 'Pass1234@!Pass1234@!Pass1234@!Pass1234@!'
                             'Pass1234@!Pass1234@!Pass1234@!Pass1234@!'
                             'Pass1234@!Pass1234@!Pass1234@!'
        }
        response = self.client.post('/password_change/', data=password)
        self.assertContains(
            response,
            "This password is too long. It must contain at most 100 characters"
        )


class InvitationTestCase(TestCase):
    fixtures = ['token.json',
                'inactive_user.json']

    def setUp(self):
        self.token = Token.objects.get(pk=1).value

    def test_access_page(self):
        response = self.client.get(
            '/accept_invitation/{}/'.format(self.token))
        self.assertEqual(response.status_code, 200)

    def test_wrong_token(self):
        response = self.client.get(
            '/accept_invitation/1028301283012/')
        self.assertEqual(response.status_code, 404)

    def test_passwords_mismatch(self):
        password = {
            'new_password1': 'Pass123!',
            'new_password2': 'Pass123@'
        }
        response = self.client.post(
            '/accept_invitation/{}/'.format(self.token),
            data=password)
        self.assertContains(
            response,
            'The two password fields didn&#39;t match.'
        )

    def test_short_password(self):
        password = {
            'new_password1': 'Pass',
            'new_password2': 'Pass'
        }
        response = self.client.post(
            '/accept_invitation/{}/'.format(self.token),
            data=password)
        self.assertContains(
            response,
            'This password is too short. It must contain at least 8 characters'
        )

    def test_no_number(self):
        password = {
            'new_password1': 'Passabcd@',
            'new_password2': 'Passabcd@'
        }
        response = self.client.post(
            '/accept_invitation/{}/'.format(self.token),
            data=password)
        self.assertContains(
            response,
            'This password has no numbers.'
        )

    def test_no_symbol(self):
        password = {
            'new_password1': 'Passabcd12',
            'new_password2': 'Passabcd12'
        }
        response = self.client.post(
            '/accept_invitation/{}/'.format(self.token),
            data=password)
        self.assertContains(
            response,
            'This password has no symbols.'
        )

    def test_no_uppercase(self):
        password = {
            'new_password1': 'passabcd12@!',
            'new_password2': 'passabcd12@!'
        }
        response = self.client.post(
            '/accept_invitation/{}/'.format(self.token),
            data=password)
        self.assertContains(
            response,
            "This password has no uppercase letters."
        )

    def test_no_lowercase(self):
        password = {
            'new_password1': 'PASSABCD12@!',
            'new_password2': 'PASSABCD12@!'
        }
        response = self.client.post(
            '/accept_invitation/{}/'.format(self.token),
            data=password)
        self.assertContains(
            response,
            "This password has no lowercase letters."
        )

    def test_long_password(self):
        password = {
            'new_password1': 'Pass1234@!Pass1234@!Pass1234@!Pass1234@!'
                             'Pass1234@!Pass1234@!Pass1234@!Pass1234@!'
                             'Pass1234@!Pass1234@!Pass1234@!',
            'new_password2': 'Pass1234@!Pass1234@!Pass1234@!Pass1234@!'
                             'Pass1234@!Pass1234@!Pass1234@!Pass1234@!'
                             'Pass1234@!Pass1234@!Pass1234@!'
        }
        response = self.client.post(
            '/accept_invitation/{}/'.format(self.token),
            data=password)
        self.assertContains(
            response,
            "Ensure this value has at most 100 characters (it has 110)."
        )
