from django.contrib.admin import ModelAdmin

from core.models import Patient
from core.models import Procedure
from core.models import Operation
from core.models import Survey
from core.models import Token
from dcodr.admin import admin_site


class ProcedureAdmin(ModelAdmin):
    list_display = ('code', 'name', 'description', 'is_active')
    list_display_links = ('code', 'name')
    list_filter = ('is_active',)

    delete_selected_confirmation_template = (
        'core/delete_selected_confirmation.html')
    delete_confirmation_template = 'core/delete_confirmation.html'


admin_site.register(Patient)
admin_site.register(Procedure, ProcedureAdmin)
admin_site.register(Operation)
admin_site.register(Survey)
admin_site.register(Token)
