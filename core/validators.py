import re
from django.utils.translation import ugettext as _
from django.core.validators import ValidationError


class UppercaseValidator:
    """
    Validates if the password has uppercase letters
    """

    def validate(self, password, user=None):
        if re.search(r"[A-Z]", password) is None:
            raise ValidationError(
                _("This password has no uppercase letters."),
                code='password_no_uppercase',
            )

    def get_help_text(self):
        return _("This password must have at least one upper case letter.")


class LowercaseValidator:
    """
    Validates if the password has lowercase letters
    """

    def validate(self, password, user=None):
        if re.search(r"[a-z]", password) is None:
            raise ValidationError(
                _("This password has no lowercase letters."),
                code='password_no_lowercase',
            )

    def get_help_text(self):
        return _("This password must have at least one lower case letter.")


class SpecialCharacterValidator:
    """
    Validates if the password has special characters
    """

    def validate(self, password, user=None):
        if re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password) is None:
            raise ValidationError(
                _("This password has no symbols."),
                code='password_no_special_character',
            )

    def get_help_text(self):
        return _("This password must have at least one symbol.")


class DigitValidator:
    """
    Validates if the password has special characters
    """

    def validate(self, password, user=None):
        if re.search(r"\d", password) is None:
            raise ValidationError(
                _("This password has no numbers."),
                code='password_no_digit',
            )

    def get_help_text(self):
        return _("This password must have at least one number.")


class MaxLengthValidator:
    """
    Validates if the password is shorter than x characters
    """

    def __init__(self, length=100):
        self.length = length

    def validate(self, password, user=None):
        if len(password) > self.length:
            raise ValidationError(
                _("This password is too long."
                  " It must contain at most 100 characters"),
                code='password_no_digit',
            )

    def get_help_text(self):
        return _("This password must have at least one number.")
