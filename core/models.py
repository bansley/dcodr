from datetime import datetime
from hashlib import md5

from django.contrib.auth.models import User
from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import DateField
from django.db.models import ForeignKey
from django.db.models import Manager
from django.db.models import Model
from django.db.models import OneToOneField
from django.db.models import QuerySet
from django.db.models import TextField
from django.db.models.signals import post_save
from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver

from core.utils import send_invitation_email


class Patient(Model):
    patient_id = CharField(max_length=30, unique=True)

    class Meta:
        ordering = ['patient_id']

    def __str__(self):
        return "Patient ID: {}".format(self.patient_id)


class Operation(Model):
    patient = ForeignKey(Patient)
    date = DateField()
    procedure = ForeignKey('Procedure', null=True)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return '{} - {}'.format(self.procedure.code, self.date)


class Survey(Model):
    operation = ForeignKey(Operation)
    date = DateField(blank=True, null=True)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return 'Survey for {} - {}'.format(self.operation, self.date)


class Token(Model):
    user = OneToOneField(User)
    value = CharField(max_length=32, null=True)

    def __str__(self):
        return self.value


class SoftDeleteQueryset(QuerySet):
    def delete(self):
        self.update(is_active=False)


class SoftDeleteManager(Manager):
    def get_active(self):
        return self.model.objects.filter(is_active=True)

    def get_queryset(self):
        return SoftDeleteQueryset(self.model, using=self._db)


class Procedure(Model):
    name = CharField(max_length=50)
    description = TextField()
    code = CharField(max_length=20)
    is_active = BooleanField()

    objects = SoftDeleteManager()

    def delete(self, *args, **kwargs):
        self.is_active = False
        self.save()

    def __str__(self):
        return '{} - {}'.format(self.code, self.description)


# ======================= SIGNALS =========================


@receiver(post_save, sender=User)
def deactivate_just_created_user(sender, instance, created, **kwargs):
    if created and not instance.is_superuser:
        instance.is_active = False
        instance.save()


@receiver(post_save, sender=User)
def invite_user_on_creation(sender, instance, created, **kwargs):
    if created and not instance.is_superuser:
        Token.objects.create(user=instance)
        send_invitation_email(instance)


@receiver(pre_save, sender=Token)
def create_token_value(sender, instance, **kwargs):
    instance.value = md5(str(datetime.now()).encode('utf8')).hexdigest()
