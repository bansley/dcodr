# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20151229_1747'),
    ]

    operations = [
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('received_date', models.DateField(null=True, blank=True)),
            ],
        ),
        migrations.AlterField(
            model_name='patient',
            name='patient_id',
            field=models.CharField(unique=True, max_length=30),
        ),
        migrations.CreateModel(
            name='EQ5Survey',
            fields=[
                ('survey_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, to='core.Survey', primary_key=True)),
                ('mobility', models.PositiveIntegerField(null=True, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)], blank=True)),
                ('self_care', models.PositiveIntegerField(null=True, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)], blank=True)),
                ('usual_activities', models.PositiveIntegerField(null=True, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)], blank=True)),
                ('pain_discomfort', models.PositiveIntegerField(null=True, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)], blank=True)),
                ('anxiety_depression', models.PositiveIntegerField(null=True, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)], blank=True)),
                ('your_health', models.PositiveIntegerField(null=True, validators=[django.core.validators.MaxValueValidator(100)], blank=True)),
            ],
            bases=('core.survey',),
        ),
    ]
