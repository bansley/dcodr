# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Operation',
            fields=[
                ('id', models.AutoField(serialize=False,
                                        primary_key=True,
                                        verbose_name='ID',
                                        auto_created=True)),
                ('date', models.DateField()),
                ('procedure_code', models.CharField(max_length=20)),
                ('patient', models.ForeignKey(to='core.Patient')),
            ],
        ),
    ]
