# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_procedure'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='operation',
            name='procedure_code',
        ),
        migrations.AddField(
            model_name='operation',
            name='procedure',
            field=models.ForeignKey(to='core.Procedure', null=True),
        ),
        migrations.AlterField(
            model_name='procedure',
            name='code',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='procedure',
            name='name',
            field=models.CharField(max_length=50),
        ),
    ]
