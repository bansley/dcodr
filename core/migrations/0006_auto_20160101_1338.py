# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20151230_2151'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='operation',
            options={'ordering': ['-date']},
        ),
        migrations.AlterModelOptions(
            name='patient',
            options={'ordering': ['patient_id']},
        ),
        migrations.AddField(
            model_name='survey',
            name='operation',
            field=models.ForeignKey(default=1, to='core.Operation'),
            preserve_default=False,
        ),
    ]
