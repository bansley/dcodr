# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.AutoField(primary_key=True,
                                        verbose_name='ID',
                                        auto_created=True,
                                        serialize=False)),
                ('patient_id', models.CharField(max_length=30)),
            ],
        ),
    ]
