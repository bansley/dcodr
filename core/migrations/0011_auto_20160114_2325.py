# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('outcomes', '0006_auto_20160114_2325'),
        ('core', '0010_auto_20160107_1949'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eq5survey',
            name='survey_ptr',
        ),
        migrations.DeleteModel(
            name='EQ5Survey',
        ),
    ]
