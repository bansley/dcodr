# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20160106_1509'),
    ]

    operations = [
        migrations.AlterField(
            model_name='token',
            name='value',
            field=models.CharField(max_length=32, null=True),
        ),
    ]
