#!/bin/bash
#install postgresql 9.4
echo "Installing postgreSQL 9.4..."
touch /etc/apt/sources.list.d/pdgd.list
echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" >> /etc/apt/sources.list.d/pdgd.list

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
apt-get update

apt-get install postgresql-9.4 postgresql-contrib-9.4 postgresql-server-dev-9.4 postgresql-client-9.4 -y

#install nginx
echo "Installing nginx..."
apt-get install nginx -y

#install virtulenv
echo "Installing pip"
apt-get install python3-pip -y

#install uwsgi
echo "Installing uwsgi..."
pip3 install uwsgi

#configure postgresql
echo "Configuring postgresql..."
mkdir /usr/lib/postgresql/9.4/data
chown postgres /usr/lib/postgresql/9.4/data
su - postgres
/usr/lib/postgresql/9.4/bin/initdb -D /usr/lib/postgresql/9.4/data
logout
