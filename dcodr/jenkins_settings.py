from .settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'decodr.db'
    }
}

INSTALLED_APPS.insert(6, 'django_jenkins')

JENKINS_TASKS = (
    'django_jenkins.tasks.run_pep8',
    'django_jenkins.tasks.run_pyflakes',
    'django_jenkins.tasks.run_flake8',
)

PROJECT_APPS = (
    'core',
)
