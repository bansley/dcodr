from django.contrib.admin import AdminSite
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy

from django.contrib.auth.models import User
from dcodr.forms import AuthUserChangeForm
from dcodr.forms import AuthUserCreateForm


class DcodrAdminSite(AdminSite):
    # Text to put at the end of each page's <title>.
    site_title = ugettext_lazy('Dcodr Admin')

    # Text to put in each page's <h1>.
    site_header = ugettext_lazy('Dcodr Admin')

    # Text to put at the top of the admin index page.
    index_title = ugettext_lazy('Dcodr administration')


class DcodrUserAdmin(UserAdmin):
    form = AuthUserChangeForm
    add_form = AuthUserCreateForm
    list_display = ('username',
                    'email',
                    'first_name',
                    'last_name',
                    'is_active')
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username',
                'email',
                'first_name',
                'last_name',
                'is_staff'
            ),
        }),
    )

admin_site = DcodrAdminSite()

admin_site.register(User, DcodrUserAdmin)
