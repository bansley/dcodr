#!/bin/bash
#Sets nginx to run the application
echo "Setting nginx to run the application..."
sed -i -- 's?*pwd?'`pwd`'?g' dcodr_nginx.conf
sudo ln -s `readlink -f dcodr_nginx.conf` /etc/nginx/sites-enabled/
sudo rm -rf /etc/nginx/sites-enabled/default

#sets uwsgi to serve the application
echo "Setting uwsgi to serve the application..."
sed -i -- 's?*pwd?'`pwd`'?g' dcodr_uwsgi.ini
sudo mkdir /etc/uwsgi/vassals -p
sudo ln -s `readlink -f dcodr_uwsgi.ini` /etc/uwsgi/vassals/

touch /tmp/dcodr_crontab
echo "@reboot /usr/local/bin/uwsgi --emperor /etc/uwsgi/vassals/" > /tmp/dcodr_crontab
sudo crontab /tmp/dcodr_crontab

#install application dependencies
echo "Installing dependencies"
sudo pip3 install -r requirements.txt

#create database
echo "Creating database..."
sudo su - postgres -c psql < ./init.sql
sudo sed -i -- 's/peer/md5/g' /etc/postgresql/9.4/main/pg_hba.conf
sudo service postgresql restart
python3 manage.py migrate
python3 manage.py createcachetable
python3 manage.py createsuperuser
