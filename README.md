Deploy
======
To deploy the application to a brand new instalation of Ubuntu 14.04, you should follow the following instructions

Getting the application
-----------------------
The application will download as a ZIP file. The naming format of the file is **dcodr-master-[identifier].zip** where *identifier* is a string.

Move the zip file to the home folder of your ubuntu server and ``cd`` into the folder

Unpack the zip file with
```
unzip dcodr-master-[identifier].zip
```
then
```
cd dcodr-master-[identifier]
```

Configuring the server
----------------------
To configure the server, inside the dcoder folder, run the following commands:
```
sudo su
./configure_server_ubuntu.sh
```
This will install the webserver.

You will get back to the terminal as the user postgres.

Execute the comand <kbd>CTRL</kbd>+<kbd>D</kbd> twice to get back to your regular user.

Configuring the application
---------------------------
As a normal user, execute:
```
./configure_application_ubuntu.sh
```
At the end of the installation process you will be prompted for the superuser name, email and password.

The application is configured.

Reboot the server and it will be up and running.
